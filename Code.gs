var ACCESS_TOKEN = "xx7d9b2bd0-7968-4f23-8455-85b0c9acf865";

//the function that loads the search page (homepage.html)
function doGet(e){
  var template = HtmlService.createHtmlOutputFromFile("homepage");
  return template;
}


//fetches result to the search query
function search(searchKey,type,generation) {
  try {
    var params = { 'method': 'POST',
                  'Content-Type': 'application/json',
                  'Accept': 'application/json',
                  'Authorization': 'Bearer '+ACCESS_TOKEN,
                  'payload' : { "q": "test"
                              }
                 }
    //https://platform.cloud.coveo.com/rest/search/v2 HTTP/1.1
    
    var url = "https://platform.cloud.coveo.com/rest/search/v2?access_token="+ACCESS_TOKEN+"&q=";
  
  //append search parameters
    if(searchKey !== "") {
      url = url + searchKey;
    }
    else {
      if(type !== "Select Type" && generation !== ""){
        url = url + '@type=="'+type+'"' +'&@gen='+generation;
        url = encodeURI(url);
      }
      else if(type !=="Select Type" && generation == "") {
        url = url + '@type=="'+type+'"';
        url = encodeURI(url)
      }
      else if(generation !== "" ){
        url = url + '@generation='+generation;
      }
    }
    
    Logger.log(url);
    
    var response = UrlFetchApp.fetch(url,params);
    
    response = response.getContentText()
    response = JSON.parse(response);
    
    var results = response.results;
    results = createResponse(results); //to create an object of the format we want to use on page
    
    //var ss = SpreadsheetApp.openById("1tVRAkAosPHhYeG6cSujginRBu2qHJ-hufYT3jFlD2WM").getSheetByName("Sheet1");
    //for(var i=0;i<results.length;i++) {
      //ss.appendRow([JSON.stringify(results[i])]);
    //}
    Logger.log(results)
    return results;
  }catch(e) {
    Logger.log(e)
    return false
  }
}

function createResponse(results) {

  var array_of_obj = new Array();
  
  for(var i=0;i<results.length; i++) {
    var obj = {};
    obj.title = results[i].title;
    obj.excerpt = results[i].excerpt;
    obj.score = results[i].score;
    obj.percentScore = results[i].percentScore;
    obj.weight = results[i].raw.weight;
    obj.concepts = results[i].raw.concepts;
    obj.description = results[i].raw.description;
    obj.ability = results[i].ability;
    obj.pokedexEntries = results[i].raw.pokedexentries;
    obj.generation = results[i].raw.generation;
    obj.type = results[i].raw.type;
    obj.imageURL = results[i].raw.imageurl;
    obj.nationalNumber = results[i].raw.nationalnumber;
    
    array_of_obj.push(obj);
  }
  return array_of_obj
}